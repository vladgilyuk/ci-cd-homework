/* eslint-disable */
import request from 'supertest';
import app, { server } from '../src/index';

describe('GET /', () => {
  let manageServer : any;
  beforeEach(function () {
    manageServer = server;
  });
  afterEach(function () {
    manageServer.close();
  });
  it('should respond with "Hello, world!"', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Hello, world!');
  });
});
