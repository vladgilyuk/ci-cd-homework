import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get("/", (req: Request, res: Response) => {
  res.send("Hello, world!");
});

export const server = app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

export default app;
